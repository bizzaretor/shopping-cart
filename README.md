# Shopping Cart Django Application 

**The api docs are available at `http://localhost:8000/swagger`**


## Checklist

- [x] Create Django models for User, Product, Order, and Payment.
- [x] Define relationships between the models (e.g., an order belongs to a user, an order contains multiple products).
- [x] Document expected behaviors of the models.
- [x] Create API views for CRUD operations (add, delete, update) for User, Product, Order, and Payment models.
- [x] Create serializers for User, Product, Order, and Payment models to convert complex data types to JSON.
- [x] Define and document any business logic required for the application (e.g., calculations, validations).
- [x] Implement basic authentication through HTTP header (static key check is acceptable).
- [x] (Bonus) Utilize Django Rest Framework for building RESTful APIs.
- [x] (Bonus) Implement JWT authentication and verification.
- [x] (Bonus) Add necessary unit tests or integration tests for API endpoints and business logic.
- [x] Submit the entire Django project to GitHub.
- [x] Include the shopping_cart app and any other necessary files.
- [x] Provide a detailed document outlining the steps you took to design the data model, integrate business logic, and implement the API. Include any challenges faced and decisions made during the development process.


### Extra Implementation
- [x] Factory boy integration
- [x] Api documentation made available via Swagger
- [x] Linting setup

## Setup

Follow these steps to get your development environment set up:

1. Clone the repository to your local machine using `git clone https://github.com/krishnanunnir/shopping_cart`.
2. Navigate to the project directory.
3. Create a new virtual environment: `python3 -m venv env`.
4. Activate the virtual environment: 
    - On Windows, run: `env\Scripts\activate`
    - On Unix or MacOS, run: `source env/bin/activate`
5. Install the required dependencies from `requirements.txt`: `pip install -r requirements.txt`.
6. Run the migrations on the project: `python manage.py migrate`.
7. Start the development server: `python manage.py runserver`.
8. Navigate to the server address in your browser. If your server is running on localhost, this would be `http://localhost:8000`.



## Folder strucutre

```
.
├── artifacts
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── shopping_cart
    ├── models
    │   ├── {model_name}.py 
    ├── views
    │   ├── {model_name}_viewset.py
    ├── serializers
    │   ├── {model_name}_serializer.py
    └── tests
        ├── views
        │   ├── test_{model_name}_viewset.py
        └── serializers
            └── test_{model_name}_serializer.py
# here models are Product, Order, Payment, Userprofile
```

## Assumptions and Business Logic

1. Order
   1. We cannot delete order, orders can only be marked as cancelled, a sort of soft delete
   2. The update should only be possible for the `order_status` of the Order object
2. Payment
   1. The creation of the Payment object would be associated with the creation of order
   2. Nothing in this model should be editable via an API, as for the status, ideally a periodic task should check for the status and update it. As this is outside the scope of the task not creating
3. Userprofile - We have defined a custom User - `UserProfile` object as it provides a fair bit of flexibility in the long run
4. Viewsets - We use viewsets for the APIs to remove redundant code and make it easy to write APIs
5. JWT based authentication - We provide JWT authentication using the simplejwt package

## Data models

```mermaid
classDiagram
    UserProfile <|-- Order : user
    Order "1"-- "*" Product : items
    Order <|-- Payment : order
    class UserProfile {
        +email
        +phone_number
        +name
        +is_active
        +is_staff
    }
    class Order {
        +order_id
        +items
        +user
        +created_at
        +order_status
    }
    class Product {
        +name
        +slug
        +price
        +description
        +image_url
    }
    class Payment {
        +id
        +amount
        +payment_status
        +created_at
        +order
    }

```
from rest_framework import serializers

from shopping_cart.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ["name", "price", "description", "image_url"]

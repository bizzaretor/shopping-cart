from rest_framework import serializers

from shopping_cart.models import Order, Payment, Product


class OrderCreateSerializer(serializers.ModelSerializer):
    items = serializers.PrimaryKeyRelatedField(
        queryset=Product.objects.all(), many=True
    )

    class Meta:
        model = Order
        fields = ["items", "order_status"]

    def validate_items(self, value):
        if not value:
            raise serializers.ValidationError("Items cannot be empty")
        return value

    def create(self, validated_data):
        items = self.validated_data.pop("items")
        order = super().create(validated_data)
        total_amount = 0
        for product in items:
            total_amount += product.price
        Payment.objects.create(order=order, amount=total_amount)
        return order


class OrderUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ["order_status"]


class OrderViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ["items", "order_status", "user"]

import uuid

from django.db import models


class Order(models.Model):
    order_status_choices = (
        ("pending", "Pending"),
        ("dispatched", "Dispatched"),
        ("out_for_delivery", "Out for delivery"),
        ("delivered", "Delivered"),
        ("cancelled", "Cancelled"),
    )
    order_id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    items = models.ManyToManyField("Product")
    user = models.ForeignKey("UserProfile", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    order_status = models.CharField(
        max_length=20, choices=order_status_choices, default="pending"
    )

    def __str__(self):
        return f"Order ID: {self.order_id}"

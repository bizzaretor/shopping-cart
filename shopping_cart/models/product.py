from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    price = models.DecimalField(max_digits=5, decimal_places=2)
    description = models.TextField()
    image_url = models.URLField(max_length=2083)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f"/product/{self.slug}"

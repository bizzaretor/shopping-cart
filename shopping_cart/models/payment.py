from django.db import models


class Payment(models.Model):
    """Any payments made by the user will be stored in this model.

    Negative payment amount will indicate that the payment was refunded.

    Making an assumption the actual payout is done via third party
    """

    payment_status_choices = (
        ("pending", "Pending"),
        ("completed", "Completed"),
        ("failed", "Failed"),
    )
    order = models.ForeignKey("Order", on_delete=models.SET_NULL, blank=True, null=True)
    amount = models.FloatField()
    payment_status = models.CharField(
        max_length=20, choices=payment_status_choices, default="pending"
    )
    created_at = models.DateTimeField(auto_now_add=True)

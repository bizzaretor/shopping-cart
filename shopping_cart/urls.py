from django.urls import include, path
from rest_framework.routers import DefaultRouter

from shopping_cart import views

router = DefaultRouter()
router.register(r"product", views.ProductViewSet)
router.register(r"order", views.OrderViewSet)
router.register(r"payment", views.PaymentViewSet)
router.register(r"user", views.UserProfileViewSet)

urlpatterns = [
    path("", include(router.urls)),
]

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from shopping_cart.models import Product
from shopping_cart.serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

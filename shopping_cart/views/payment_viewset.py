from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from shopping_cart.models import Payment
from shopping_cart.serializers import PaymentSerializer


class PaymentViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer

from rest_framework import permissions, viewsets

from shopping_cart.models import UserProfile
from shopping_cart.serializers import UserProfileCreateSerializer, UserProfileSerializer


class IsSuperUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_superuser


class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()

    def get_permissions(self):
        if self.action == "list" or self.action == "delete":
            permission_classes = [IsSuperUser()]
        elif self.action == "update":
            permission_classes = [permissions.IsAuthenticated()]
        else:
            permission_classes = [permissions.AllowAny()]
        return permission_classes

    def get_serializer_class(self):
        if self.action == "create":
            return UserProfileCreateSerializer
        return UserProfileSerializer

from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated

from shopping_cart.models import Order
from shopping_cart.serializers import (
    OrderCreateSerializer,
    OrderUpdateSerializer,
    OrderViewSerializer,
)


class OrderViewSet(
    viewsets.GenericViewSet,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
):
    """
    This viewset should be able to do everything except deleting an order
    """

    permission_classes = [IsAuthenticated]
    queryset = Order.objects.all()

    def get_serializer_class(self):
        if self.action == "list" or self.action == "retrieve":
            return OrderViewSerializer
        elif self.action == "create":
            return OrderCreateSerializer
        else:
            return OrderUpdateSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

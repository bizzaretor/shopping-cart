from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from shopping_cart.models import Product
from shopping_cart.tests.factories import ProductFactory, UserProfileFactory


class TestProductViewSet(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.product1 = ProductFactory()
        self.product2 = ProductFactory()
        self.user = UserProfileFactory()
        self.client.force_authenticate(user=self.user)

    def test_permission__logged_in_user(self):
        new_user = UserProfileFactory()
        self.client.force_authenticate(user=new_user)
        response = self.client.get(reverse("product-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_permission__anonymous_user(self):
        self.client.logout()
        response = self.client.get(reverse("product-list"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_products(self):
        response = self.client.get(reverse("product-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_retrieve_product(self):
        response = self.client.get(
            reverse("product-detail", kwargs={"pk": self.product1.id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["name"], self.product1.name)

    def test_create_product(self):
        data = {
            "name": "Test Product",
            "price": "100.0",
            "description": "Test Description",
            "image_url": "http://test.com/image.jpg",
        }
        response = self.client.post(reverse("product-list"), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.count(), 3)

    def test_update_product(self):
        data = {
            "name": "Updated Product",
            "price": "200.0",
            "description": "Updated Description",
            "image_url": "http://test.com/updated.jpg",
        }
        response = self.client.put(
            reverse("product-detail", kwargs={"pk": self.product1.id}), data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.product1.refresh_from_db()
        self.assertEqual(self.product1.name, "Updated Product")
        self.assertEqual(self.product1.price, 200.0)
        self.assertEqual(self.product1.description, "Updated Description")
        self.assertEqual(self.product1.image_url, "http://test.com/updated.jpg")

    def test_delete_product(self):
        response = self.client.delete(
            reverse("product-detail", kwargs={"pk": self.product1.id})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Product.objects.count(), 1)

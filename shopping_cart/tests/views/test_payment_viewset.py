from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from shopping_cart.tests.factories import PaymentFactory, UserProfileFactory


class TestPaymentViewSet(APITestCase):
    def setUp(self):
        self.payment1 = PaymentFactory()
        self.payment2 = PaymentFactory()
        self.client = APIClient()
        self.user = UserProfileFactory()
        self.client.force_authenticate(user=self.user)

    def test_permission__logged_in_user(self):
        new_user = UserProfileFactory()
        self.client.force_authenticate(user=new_user)
        response = self.client.get(reverse("payment-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_permission__anonymous_user(self):
        self.client.logout()
        response = self.client.get(reverse("payment-list"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_payments(self):
        PaymentFactory()
        PaymentFactory()
        response = self.client.get(reverse("payment-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)

    def test_retrieve_payment(self):
        response = self.client.get(
            reverse("payment-detail", kwargs={"pk": self.payment1.id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["amount"], self.payment1.amount)

    def test_create_payment(self):
        data = {
            "amount": "100.0",
            "payment_method": "Credit Card",
            "status": "Completed",
        }
        response = self.client.post(reverse("payment-list"), data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_update_payment(self):
        data = {"status": "Failed"}
        response = self.client.put(
            reverse("payment-detail", kwargs={"pk": self.payment1.id}), data
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete_payment(self):
        response = self.client.delete(
            reverse("payment-detail", kwargs={"pk": self.payment1.id})
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

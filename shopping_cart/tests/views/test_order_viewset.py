from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from shopping_cart.models import Order
from shopping_cart.tests.factories import (
    OrderFactory,
    ProductFactory,
    UserProfileFactory,
)


class TestOrderViewSet(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.product1 = ProductFactory()
        self.product2 = ProductFactory()
        self.user = UserProfileFactory()
        self.order = OrderFactory(items=[self.product1, self.product2], user=self.user)
        self.client.force_authenticate(user=self.user)

    def test_permission__logged_in_user(self):
        new_user = UserProfileFactory()
        self.client.force_authenticate(user=new_user)
        response = self.client.get(
            reverse("order-detail", kwargs={"pk": self.order.order_id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_permission__anonymous_user(self):
        self.client.logout()
        response = self.client.get(
            reverse("order-detail", kwargs={"pk": self.order.order_id})
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_orders(self):
        response = self.client.get(reverse("order-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_retrieve_order(self):
        response = self.client.get(
            reverse("order-detail", kwargs={"pk": self.order.order_id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["items"], [self.product1.id, self.product2.id])
        self.assertEqual(response.data["order_status"], "pending")
        self.assertEqual(response.data["user"], self.user.id)

    def test_create_order(self):
        data = {
            "items": [self.product1.id, self.product2.id],
            "order_status": "pending",
        }
        response = self.client.post(reverse("order-list"), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Order.objects.count(), 2)

    def test_update_order(self):
        data = {"order_status": "dispatched"}
        response = self.client.put(
            reverse("order-detail", kwargs={"pk": self.order.order_id}), data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.order.refresh_from_db()
        self.assertEqual(self.order.order_status, "dispatched")

# FILE: TEST_USER_PROFILE_VIEWSET.PY
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from shopping_cart.models import UserProfile
from shopping_cart.tests.factories import UserProfileFactory


class TestUserProfileViewSet(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = UserProfileFactory()
        self.superuser = UserProfileFactory(is_superuser=True)
        self.client.force_authenticate(user=self.user)

    def test_list_users(self):
        self.client.force_authenticate(user=self.superuser)
        response = self.client.get(reverse("userprofile-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), UserProfile.objects.count())

    def test_retrieve_user(self):
        response = self.client.get(
            reverse("userprofile-detail", kwargs={"pk": self.user.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["email"], self.user.email)

    def test_create_user(self):
        data = {
            "email": "test2@example.com",
            "phone_number": "+1234567891",
            "name": "Test User 2",
            "password": "testpassword123",
        }
        response = self.client.post(reverse("userprofile-list"), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(UserProfile.objects.count(), 3)

    def test_update_user(self):
        data = {
            "name": "Updated Name",
            "email": "test@gmail.com",
            "phone_number": "+1234567890",
        }
        response = self.client.put(
            reverse("userprofile-detail", kwargs={"pk": self.user.pk}), data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertEqual(self.user.name, "Updated Name")

    def test_delete_user(self):
        self.client.force_authenticate(user=self.superuser)
        response = self.client.delete(
            reverse("userprofile-detail", kwargs={"pk": self.user.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(UserProfile.objects.count(), 1)

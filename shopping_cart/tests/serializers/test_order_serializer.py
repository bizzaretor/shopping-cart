from django.test import TestCase

from shopping_cart.models import Payment
from shopping_cart.serializers import (
    OrderCreateSerializer,
    OrderUpdateSerializer,
    OrderViewSerializer,
)
from shopping_cart.tests.factories import (
    OrderFactory,
    PaymentFactory,
    ProductFactory,
    UserProfileFactory,
)


class TestOrderSerializer(TestCase):
    def setUp(self):
        self.product1 = ProductFactory()
        self.product2 = ProductFactory()
        self.order = OrderFactory(items=[self.product1, self.product2])
        self.payment = PaymentFactory(order=self.order)

    def test_order_create_serializer__valid(self):
        serializer = OrderCreateSerializer(
            data={
                "items": [self.product1.id, self.product2.id],
                "order_status": "pending",
            }
        )
        self.assertTrue(serializer.is_valid())

    def test_order_create_serializer__empty_items__invalid(self):
        serializer = OrderCreateSerializer(
            data={"items": [], "order_status": "pending"}
        )
        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors.keys()), set(["items"]))

    def test_order_create_serializer__invalid_order_status__invalid(self):
        serializer = OrderCreateSerializer(
            data={
                "items": [self.product1.id, self.product2.id],
                "order_status": "invalid",
            }
        )
        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors.keys()), set(["order_status"]))

    def test_order_create_serializer__invalid_items__invalid(self):
        serializer = OrderCreateSerializer(
            data={
                "items": [999],
                "order_status": "pending",
            }
        )
        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors.keys()), set(["items"]))

    def test_order_create_serializer__create_method(self):
        serializer = OrderCreateSerializer(
            data={
                "items": [self.product1.id, self.product2.id],
                "order_status": "pending",
            }
        )
        self.assertTrue(serializer.is_valid())
        user = UserProfileFactory()
        order = serializer.save(user=user)
        self.assertEqual(order.items.count(), 2)
        self.assertEqual(order.order_status, "pending")
        payment = Payment.objects.get(order=order)
        self.assertEqual(payment.amount, self.product1.price + self.product2.price)

    def test_order_update_serializer__valid_data(self):
        serializer = OrderUpdateSerializer(data={"order_status": "pending"})
        self.assertTrue(serializer.is_valid())

    def test_order_update_serializer__invalid_order_status__invalid(self):
        serializer = OrderUpdateSerializer(data={"order_status": "invalid"})
        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors.keys()), set(["order_status"]))

    def test_order_view_serializer(self):
        serializer = OrderViewSerializer(self.order)
        data = serializer.data
        self.assertEqual(set(data.keys()), set(["items", "order_status", "user"]))

from django.test import TestCase

from shopping_cart.serializers import PaymentSerializer
from shopping_cart.tests.factories import OrderFactory, PaymentFactory


class TestPaymentSerializer(TestCase):
    def setUp(self):
        self.order = OrderFactory()
        self.payment = PaymentFactory(order=self.order)

    def test_payment_serializer__valid(self):
        serializer = PaymentSerializer(self.payment)
        data = serializer.data
        self.assertEqual(
            set(data.keys()),
            set(["id", "order", "amount", "payment_status", "created_at"]),
        )
        self.assertEqual(data["id"], self.payment.id)
        self.assertEqual(data["order"], self.payment.order.order_id)
        self.assertEqual(float(data["amount"]), float(self.payment.amount))
        self.assertEqual(data["payment_status"], self.payment.payment_status)

    def test_payment_serializer__create_method(self):
        serializer = PaymentSerializer(
            data={
                "order": self.order.order_id,
                "amount": 100.0,
                "payment_status": "pending",
                "created_at": "2021-01-01T00:00:00Z",
            }
        )
        self.assertTrue(serializer.is_valid())
        payment = serializer.save()
        self.assertEqual(payment.amount, 100.0)
        self.assertEqual(payment.payment_status, "pending")
        self.assertEqual(payment.order, self.order)

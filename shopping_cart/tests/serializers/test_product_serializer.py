from decimal import Decimal

from django.test import TestCase

from shopping_cart.serializers import ProductSerializer
from shopping_cart.tests.factories import ProductFactory


class TestProductSerializer(TestCase):
    def setUp(self):
        self.product = ProductFactory()

    def test_product_serializer_serialization(self):
        serializer = ProductSerializer(self.product)
        data = serializer.data
        self.assertEqual(
            set(data.keys()), set(["name", "description", "price", "image_url"])
        )
        self.assertEqual(data["name"], self.product.name)
        self.assertEqual(data["description"], self.product.description)
        self.assertEqual(float(data["price"]), float(self.product.price))
        self.assertEqual(data["image_url"], self.product.image_url)

    def test_product_serializer_validation_and_creation(self):
        serializer = ProductSerializer(
            data={
                "name": "Test Product",
                "price": "100.0",
                "description": "Test Description",
                "image_url": "http://test.com/image.jpg",
            }
        )
        self.assertTrue(serializer.is_valid())
        product = serializer.save()
        self.assertEqual(product.name, "Test Product")
        self.assertEqual(product.price, Decimal("100.0"))
        self.assertEqual(product.description, "Test Description")
        self.assertEqual(product.image_url, "http://test.com/image.jpg")

    def test_product_serializer_invalid_price(self):
        serializer = ProductSerializer(
            data={
                "name": "Test Product",
                "price": "invalid",
                "description": "Test Description",
                "image_url": "http://test.com/image.jpg",
            }
        )
        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors.keys()), set(["price"]))

    def test_product_serializer_invalid_image_url(self):
        serializer = ProductSerializer(
            data={
                "name": "Test Product",
                "price": "100.0",
                "description": "Test Description",
                "image_url": "invalid",
            }
        )
        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors.keys()), set(["image_url"]))

    def test_product_serializer_empty_name(self):
        serializer = ProductSerializer(
            data={
                "name": "",
                "price": "100.0",
                "description": "Test Description",
                "image_url": "http://test.com/image.jpg",
            }
        )
        self.assertFalse(serializer.is_valid())
        self.assertEqual(set(serializer.errors.keys()), set(["name"]))

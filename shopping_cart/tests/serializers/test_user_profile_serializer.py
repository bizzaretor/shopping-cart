from django.test import TestCase

from shopping_cart.models import UserProfile
from shopping_cart.serializers import UserProfileCreateSerializer, UserProfileSerializer


class TestUserProfileCreateSerializer(TestCase):
    def setUp(self):
        self.valid_payload = {
            "email": "test@example.com",
            "phone_number": "+1234567890",
            "name": "Test User",
            "password": "testpassword123",
        }

    def test_create_user_profile(self):
        serializer = UserProfileCreateSerializer(data=self.valid_payload)
        self.assertTrue(serializer.is_valid())
        user = serializer.save()
        self.assertEqual(user.email, self.valid_payload["email"])
        self.assertEqual(user.phone_number, self.valid_payload["phone_number"])
        self.assertEqual(user.name, self.valid_payload["name"])
        self.assertTrue(user.check_password(self.valid_payload["password"]))

    def test_password_write_only(self):
        serializer = UserProfileCreateSerializer(data=self.valid_payload)
        self.assertTrue(serializer.is_valid())
        data = serializer.data
        self.assertNotIn("password", data)


class TestUserProfileSerializer(TestCase):
    def setUp(self):
        self.user_profile = UserProfile.objects.create(
            email="test@example.com",
            phone_number="+1234567890",
            name="Test User",
        )
        self.serializer = UserProfileSerializer(instance=self.user_profile)

    def test_contains_expected_fields(self):
        data = self.serializer.data
        self.assertSetEqual(set(data.keys()), set(["email", "phone_number", "name"]))

    def test_field_content(self):
        data = self.serializer.data
        self.assertEqual(data["email"], self.user_profile.email)
        self.assertEqual(data["phone_number"], self.user_profile.phone_number)
        self.assertEqual(data["name"], self.user_profile.name)

import factory
from factory.django import DjangoModelFactory

from shopping_cart.models import Order, Payment, Product


class ProductFactory(DjangoModelFactory):
    class Meta:
        model = Product

    name = factory.Faker("word")
    price = factory.Faker("random_int", min=1, max=1000)


class UserProfileFactory(DjangoModelFactory):
    class Meta:
        model = "shopping_cart.UserProfile"

    email = factory.Faker("email")
    phone_number = factory.Faker("phone_number")
    name = factory.Faker("name")
    password = factory.Faker("password")


class OrderFactory(DjangoModelFactory):
    class Meta:
        model = Order

    user = factory.SubFactory(UserProfileFactory)

    @factory.post_generation
    def items(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for item in extracted:
                self.items.add(item)


class PaymentFactory(DjangoModelFactory):
    class Meta:
        model = Payment

    order = factory.SubFactory(OrderFactory)
    amount = factory.Faker("random_int", min=1, max=1000)
